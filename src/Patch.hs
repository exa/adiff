module Patch
  ( patchToks
  , pprPatchWarn
  ) where

import qualified Data.ByteString as B
import Data.Maybe (catMaybes)
import qualified Data.Vector as V
import Format
import Merge
import Types

data PatchWarn
  = HunkMatched Hunk (Int, Int)
  | HunkFailed Hunk (Int, Int)
  deriving (Show)

data PatchOpts =
  PatchOpts
    { scanRange :: Int
    , minContext :: Int
    , mergeOpts :: MergeOpts
    }

data PatchState =
  PatchState
    { input :: TV
    , output :: [TV]
    , warns :: [PatchWarn]
    , inOff :: Int -- where we are in `input`
    , patchInOff :: Int -- to what position does that correspond in patch input (gets adjusted on fuzzy matches)
    , outOff :: Int -- where we are in output tokens (informative)
    , patchOutOff :: Int -- to what position does that correspond in patch output (gets adjusted from processed hunks)
    }

pprPatchWarn :: PatchWarn -> String
pprPatchWarn (HunkMatched (offs, _) poffs) =
  "hunk (" ++ pprOffs offs ++ ") succeeded at " ++ pprOffs poffs
pprPatchWarn (HunkFailed (offs, _) poffs) =
  "hunk (" ++ pprOffs offs ++ ") FAILED, expected at " ++ pprOffs poffs

pprOffs :: (Int, Int) -> String
pprOffs (o, n) = "-" ++ show o ++ " +" ++ show n

patchToks ::
     TV
  -> [Hunk]
  -> Bool
  -> Int
  -> Int
  -> MergeOpts
  -> (TV, [Hunk], [PatchWarn])
patchToks toks hunks revPatch scan ctxt mopt =
  go hunks $ PatchState toks [] [] 0 0 0 0
  where
    go [] ps =
      ( V.concat (output ps ++ [V.drop (inOff ps) (input ps)])
      , [rej | HunkFailed rej _ <- warns ps]
      , warns ps)
    go (h:hs) ps = go hs ps'
      where
        ((fromPos, toPos), diff) = h
        advance = fromPos - patchInOff ps
        noMatch =
          ps
            { warns =
                warns ps ++
                [HunkFailed h (advance + inOff ps, advance + outOff ps)]
            , patchOutOff = patchOutOff ps - diffOffChange diff
            }
        cleanMatch :: Maybe PatchState
        cleanMatch = patchHunkClean ps h mopt
        isContext :: (Op, Tok) -> Bool
        isContext (op, _) = op == Keep
        discardedContextDiffs :: [(Int, Diff)]
        discardedContextDiffs =
          let (fwdCtxt, d') = span isContext diff
              (revCtxt, revMid) = span isContext (reverse d')
              mid = reverse revMid
              discards n c@(_:r) = (n, c) : discards (n + 1) r
              discards n [] = (n, []) : discards n []
           in zipWith
                (\(dfwd, fwd) (drev, rev) ->
                   (max dfwd drev, fwd ++ mid ++ reverse rev))
                (discards 0 fwdCtxt)
                (discards 0 revCtxt)
        fuzzyHunks :: [Hunk]
        fuzzyHunks = do
          (discarded, ddiff) <- take (ctxt + 1) discardedContextDiffs
          off <- 0 : concatMap (\x -> [-x, x]) [1 .. scan]
          pure ((fromPos + discarded + off, toPos + discarded), ddiff)
        fuzzyMatches =
          [ (\x -> x {warns = warns x ++ [HunkMatched h fPos]}) <$>
          patchHunkClean ps fh mopt
          | fh@(fPos, _) <- tail fuzzyHunks -- tail omits the "clean" 0,0 one
          ]
        ps' = head $ catMaybes (cleanMatch : fuzzyMatches) ++ [noMatch]

patchHunkClean :: PatchState -> Hunk -> MergeOpts -> Maybe PatchState
patchHunkClean ps ((fromPos, toPos), diff) mopts
  | expInOff < 0 || expOutOff < 0 = Nothing
  | mergeIgnoreWhitespace mopts && whitespaceOnly diff = Just ps
  | Just repl <- matchDiff mopts (V.toList origPart) diff =
    Just
      ps
        { output = output ps ++ [skipped, V.fromList repl]
        , inOff = expInOff + matchLen
        , patchInOff = fromPos + matchLen
        , outOff = expOutOff + length repl
        , patchOutOff = toPos + length repl
        }
  | otherwise = Nothing
  where
    matchLen = diffMatchLen diff
    advance = fromPos - patchInOff ps
    expInOff = advance + inOff ps
    expOutOff = advance + outOff ps
    skipped = V.take expInOff $ input ps
    origPart = V.take matchLen $ V.drop expInOff $ input ps

whitespaceOnly :: Diff -> Bool
whitespaceOnly = all wsOnly
  where
    wsOnly (Keep, _) = True
    wsOnly (Original, _) = True
    wsOnly (_, (False, _)) = True
    wsOnly _ = False

diffMatchLen :: Diff -> Int
diffMatchLen = sum . map (off . fst)
  where
    off Keep = 1
    off Remove = 1
    off Original = 1
    off _ = 0

diffOffChange :: Diff -> Int
diffOffChange = sum . map (off . fst)
  where
    off Add = 1
    off Remove = -1
    off _ = 0

matchDiff :: MergeOpts -> [Tok] -> Diff -> Maybe [Tok]
matchDiff mopt ts ds
  | null ts
  , null ds = return []
  | ((op, tok):ds') <- ds
  , op == Add {-, MineChanged, YourChanged -- TODO special treatment needed -}
   = (tok :) <$> matchDiff mopt ts ds'
  | (intok:ts') <- ts
  , ((op, tok):ds') <- ds
  , op == Keep {-, Original -- TODO special treatment needed-}
  , tokCmp' mopt intok tok =
    (:)
      (if mergeKeepWhitespace mopt && not (fst intok)
         then intok
         else tok) <$>
    matchDiff mopt ts' ds'
  | (intok:ts') <- ts
  , ((op, tok):ds') <- ds
  , op == Remove
  , tokCmp' mopt intok tok = matchDiff mopt ts' ds'
  | otherwise = Nothing

tokCmp' :: MergeOpts -> Tok -> Tok -> Bool
tokCmp' MergeOpts {mergeForceWhitespace = x} = tokCmp x

tokCmp :: Bool -> Tok -> Tok -> Bool
tokCmp False (False, _) (False, _) = True -- do not force rejecting on whitespace change
tokCmp _ a b = a == b -- otherwise just compare
