module Main where

import Control.Exception
import Control.Monad
import qualified Data.ByteString as B
import qualified Data.ByteString.Builder as BB
import Data.Foldable (traverse_)
import qualified Data.Vector as V
import Diff
import Diff3
import Format
import Hunks
import Merge
import Options.Applicative
import Patch
import System.Exit
import System.IO (hPutStrLn, stderr, stdout)
import System.IO.MMap
import Tokenizers
import Types
import Version

data ADiffOptions =
  ADiffOptions
    { adiffTokOpts :: TokOpts
    , adiffCmdOpts :: ADiffCommandOpts
    }
  deriving (Show)

data ADiffCommandOpts
  = CmdDiff
      { context :: Int
      , diffFile1 :: String
      , diffFile2 :: String
      }
  | CmdPatch
      { patchDryRun :: Bool
      --, patchInDir :: Maybe String
      --, patchPathStrip :: Int
      , patchInputPatch :: String
      , patchOutput :: String
      , patchReject :: String --todo convert to Maybes with optional
      , patchBackup :: String
      , patchReverse :: Bool
      , patchScanRange :: Int
      , context :: Int
      , patchMergeOpts :: MergeOpts
      , patchInput :: String
      }
  | CmdDiff3
      { context :: Int
      , diff3Mine :: String
      , diff3Old :: String
      , diff3Yours :: String
      , diff3MergeOpts :: MergeOpts
      }
  deriving (Show)

contextOpt :: Bool -> Parser Int
contextOpt forPatch =
  check <$>
  option
    auto
    (metavar "CONTEXT" <>
     short 'C' <>
     long "context" <>
     value
       (if forPatch
          then 4
          else 8) <>
     help
       (if forPatch
          then "Maximum number of context tokens that may be discarded from the beginning and end of the hunk when attempting to find a match"
          else "How many tokens around the change to include in the patch"))
  where
    check c
      | c < 0 = error "Negative context"
      | otherwise = c

diffCmdOptions :: Parser ADiffCommandOpts
diffCmdOptions =
  CmdDiff <$> contextOpt False <*> strArgument (metavar "FROMFILE") <*>
  strArgument (metavar "TOFILE")

patchCmdOptions :: Parser ADiffCommandOpts
patchCmdOptions =
  CmdPatch <$>
  switch
    (short 'n' <>
     long "dry-run" <>
     help "Do not patch anything, just print what would happen") <*>
  -- optional (strOption $ short 'd' <> long "directory" <> metavar "DIR" <> help "Work in DIR") <*>
  -- option auto (short 'p' <> long "strip" <> metavar "NUM" <> help "Strip NUM leading components from the paths" <> value 0) <*>
  strOption
    (short 'i' <>
     long "input" <>
     metavar "PATCHFILE" <>
     help "Read the patchfile from PATCHFILE, defaults to `-' for STDIN" <>
     value "-") <*>
  strOption
    (short 'o' <>
     long "output" <>
     metavar "OUTPUT" <>
     help
       "Write the patched file to OUTPUT, use `-' for STDOUT. By default, INPUT is rewritten." <>
     value "") <*>
  strOption
    (short 'r' <>
     long "reject" <>
     metavar "REJECTS" <>
     help
       "Write the rejected hunks file to file REJECTS, instead of default `OUTPUT.rej'. Use `-' to discard rejects." <>
     value "") <*>
  strOption
    (short 'b' <>
     long "backup" <>
     metavar "BACKUP" <>
     help
       "When rewriting INPUT after a partially applied or otherwise suspicious patch, back up the original file in BACKUP instead of default `INPUT.orig'. Use `-' to discard backups." <>
     value "") <*>
  switch (short 'R' <> long "reverse" <> help "Unapply applied patches") <*>
  option
    auto
    (short 'S' <>
     long "scan-range" <>
     metavar "RANGE" <>
     help
       "Maximum distance from the indended patch position (in tokens) for fuzzy matching of hunks" <>
     value 42) <*>
  contextOpt True <*>
  mergeOption True <*>
  strArgument (metavar "INPUT")

diff3CmdOptions :: Parser ADiffCommandOpts
diff3CmdOptions =
  CmdDiff3 <$> contextOpt False <*> strArgument (metavar "MYFILE") <*>
  strArgument (metavar "OLDFILE") <*>
  strArgument (metavar "YOURFILE") <*>
  mergeOption False

actionOptions :: Parser ADiffCommandOpts
actionOptions =
  hsubparser $
  mconcat
    [ command "diff" $ info diffCmdOptions $ progDesc "Compare two files"
    , command "patch" $ info patchCmdOptions $ progDesc "Apply a patch to files"
    , command "diff3" $
      info diff3CmdOptions $ progDesc "Compare and merge three files"
    ]

adiffOptions :: Parser ADiffOptions
adiffOptions = ADiffOptions <$> tokOptions <*> actionOptions

-- TODO: load in case it's not a regular file
loadToksMM :: TokOpts -> FilePath -> IO TV
loadToksMM topt fn = loadToksWith topt fn (mmapFileByteString fn Nothing)

loadToksR :: TokOpts -> FilePath -> IO TV
loadToksR topt fn = loadToksWith topt fn (B.readFile fn)

loadToksWith :: TokOpts -> FilePath -> IO BS -> IO TV
loadToksWith topt fn bs = V.fromList <$> (bs >>= tokenize topt fn)

doDiff :: TokOpts -> ADiffCommandOpts -> IO ()
doDiff topt (CmdDiff {diffFile1 = f1, diffFile2 = f2, context = ctxt}) = do
  [toks1, toks2] <- traverse (loadToksMM topt) [f1, f2]
  let output = hunks (max 0 ctxt) $ diffToks toks1 toks2
  BB.hPutBuilder stdout $ pprHunks output
  unless (null output) $ exitWith (ExitFailure 1)
doDiff _ _ = error "dispatch failure"

doDiff3 :: TokOpts -> ADiffCommandOpts -> IO ()
doDiff3 topt (CmdDiff3 ctxt f1 f2 f3 mo) = do
  [toksMine, toksOld, toksYour] <- traverse (loadToksMM topt) [f1, f2, f3]
  let d3 = diff3Toks mo toksMine toksOld toksYour
      isConflict (MineChanged, _) = True
      isConflict (YourChanged, _) = True
      isConflict _ = False
      hasConflict = any isConflict d3
  BB.hPutBuilder stdout $
    if mergeDoMerge mo
      then do
        fmtMerged mo d3
      else pprHunks $ hunks (max 0 ctxt) d3
  when hasConflict $ exitWith (ExitFailure 1)
doDiff3 _ _ = error "dispatch failure"

note :: String -> IO ()
note = hPutStrLn stderr

doPatch :: TokOpts -> ADiffCommandOpts -> IO ()
doPatch topt o = do
  toksIn <- loadToksR topt (patchInput o)
  hs' <-
    parsePatch <$>
    case (patchInputPatch o) of
      "-" -> B.getContents
      fn -> B.readFile fn
  hs <-
    case hs' of
      Left _ -> ioError $ userError "PATCHFILE parsing failed"
      Right x -> pure x
  let (toks, rej, warns) =
        patchToks
          toksIn
          hs
          (patchReverse o)
          (patchScanRange o)
          (context o)
          (patchMergeOpts o)
      sus = not (null warns)
      dry = patchDryRun o
      rewritingInput = null (patchOutput o)
      outputStdout = patchOutput o == "-"
      rejFile
        | rewritingInput || outputStdout = patchInput o ++ ".rej"
        | otherwise = patchOutput o ++ ".rej"
      backupFile
        | patchBackup o == "-" = ""
        | patchBackup o == "" && rewritingInput = patchInput o ++ ".orig"
        | otherwise = patchBackup o
      outFile
        | rewritingInput = patchInput o
        | otherwise = patchOutput o
  traverse_ (note . pprPatchWarn) warns
  when dry $
    note $
    (if not sus
       then "OK"
       else "Possibly problematic") ++
    " patch with " ++ show (length rej :: Int) ++ " rejected hunks"
  when (not (null rej)) $
    if dry
      then note $ "Would write rejected hunks to " ++ rejFile
      else do
        note $ "Writing rejected hunks to " ++ rejFile
        BB.writeFile rejFile (pprHunks rej)
  when (sus && not (null backupFile)) $
    if dry
      then note $ "Would write backup to " ++ backupFile
      else do
        note $ "Writing backup to " ++ backupFile
        B.readFile (patchInput o) >>= B.writeFile backupFile
  let doWrite output =
        if outputStdout
          then if dry
                 then note "Would write output to stdout"
                 else BB.hPutBuilder stdout output
          else if dry
                 then note $ "Would write output to " ++ outFile
                 else do
                   note $ "Writing output to " ++ outFile
                   BB.writeFile outFile output
  doWrite (mconcat . map (BB.byteString . snd) . V.toList $ toks)
  when (dry && not (null rej)) $ do
    note "Rejected hunks:"
    BB.hPutBuilder stdout (pprHunks rej)
  when sus $ exitWith (ExitFailure 1)

main' :: IO ()
main' =
  let opts :: ParserInfo ADiffOptions
      opts =
        info
          (adiffOptions <**> versionOption "adiff" <**> helperOption)
          (fullDesc <>
           progDesc
             "Compare, patch and merge files on arbitrarily tokenized sequences." <>
           header "adiff: arbitrary-token diff utilities")
   in do ADiffOptions {adiffTokOpts = topt, adiffCmdOpts = copt} <-
           customExecParser (prefs $ helpShowGlobals <> subparserInline) opts
         (case copt of
            CmdDiff {} -> doDiff
            CmdDiff3 {} -> doDiff3
            CmdPatch {} -> doPatch)
           topt
           copt

main :: IO ()
main =
  main' `catch`
  (\e -> do
     let err = show (e :: IOException)
     note err
     exitWith $ ExitFailure 2)
