{-# LANGUAGE TupleSections #-}

module Diff
  ( diffToks
  ) where

import qualified Data.ByteString as B
import qualified Data.Vector as V
import Types

data DiffEnv =
  DiffEnv
    { deT1 :: TV
    , deT2 :: TV
    , deS :: Int
    , deE :: Int
    , deL :: Int
    , deW :: Int
    , deA :: Int
    , deB :: Int
    , deVS :: V.Vector (Int, Int)
    , deVE :: V.Vector (Int, Int)
    , deTokPrio :: Tok -> Int
    , deTrans :: Bool
    }

toksMatch :: Int -> Int -> DiffEnv -> Bool
toksMatch x y DiffEnv {deT1 = t1, deT2 = t2} = t1 V.! x == t2 V.! y

stripEqToks :: TV -> TV -> (Diff, Diff, TV, TV)
stripEqToks t1 t2 = (pre, post, t1', t2')
  where
    l1 = V.length t1
    l2 = V.length t2
    firstDiff i
      | i < l1 && i < l2 && (t1 V.! i == t2 V.! i) = firstDiff (i + 1)
      | otherwise = i
    b = firstDiff 0
    lastDiff i
      | l1 - i - 1 >= b &&
          l2 - i - 1 >= b && t1 V.! (l1 - i - 1) == t2 V.! (l2 - i - 1) =
        lastDiff (i + 1)
      | otherwise = i
    e = lastDiff 0
    pre = map (Keep, ) . V.toList . V.take b $ t1
    post = map (Keep, ) . V.toList . V.drop (l1 - e) $ t1
    t1' = V.slice b (l1 - e - b) t1
    t2' = V.slice b (l2 - e - b) t2

makePrios :: TV -> TV -> (Bool, BS) -> Int
makePrios _ _ = get
  where
    get (isToken, str) =
      if isToken
        then B.length str
        else 0

diffToks :: TV -> TV -> Diff
diffToks t1' t2' = pre ++ res ++ post
  where
    (pre, post, t1, t2) = stripEqToks t1' t2'
    stats = makePrios t1' t2'
    res
      | V.null t1 = map (Add, ) (V.toList t2)
      | V.null t2 = map (Remove, ) (V.toList t1)
      | V.length t1 >= V.length t2 =
        diffToks' $
        DiffEnv
          { deT1 = t1
          , deT2 = t2
          , deS = 0
          , deE = V.length t1
          , deL = V.length t1
          , deW = V.length t2
          , deA = 0
          , deB = V.length t2
          , deVS = V.fromList $ zip [0 .. V.length t2] (repeat 0)
          , deVE = V.fromList $ reverse $ zip [0 .. V.length t2] (repeat 0)
          , deTokPrio = stats
          , deTrans = False
          }
      | otherwise =
        diffToks' $
        DiffEnv
          { deT1 = t2
          , deT2 = t1
          , deS = 0
          , deE = V.length t2
          , deL = V.length t2
          , deW = V.length t1
          , deA = 0
          , deB = V.length t1
          , deVS = V.fromList $ zip [0 .. V.length t1] (repeat 0)
          , deVE = V.fromList $ reverse $ zip [0 .. V.length t1] (repeat 0)
          , deTokPrio = stats
          , deTrans = True
          }

minIndexFwd :: V.Vector (Int, Int) -> Int
minIndexFwd =
  V.minIndexBy
    (\x y ->
       if x <= y
         then LT
         else GT --basically normal V.minIndex
     )

minIndexRev :: V.Vector (Int, Int) -> Int
minIndexRev =
  V.minIndexBy
    (\x y ->
       if x < y
         then LT
         else GT --picks the last minimum
     )

diffToks' :: DiffEnv -> Diff
diffToks' de@DiffEnv {deS = s, deE = e, deA = a, deB = b, deTrans = trans} =
  diff
  where
    mid = quot (s + e) 2
    vecSmid = vecS mid
    vecEmid = vecE mid
    prio i = negate . deTokPrio de $ deT1 de V.! i
    vecS = vec -- "forward" operation
      where
        vec i
          | i == s = deVS de
          | i > s = V.fromList . upd i . vec $ pred i
          | otherwise = error "Internal bounds check failure"
        upd i v = (i, 0) : go 1 (i, 0)
          where
            go j (iup, sup)
              | j > deW de = []
              | otherwise =
                let (ileft, sleft) = v V.! j
                    (iupleft, supleft) = v V.! pred j
                    keep
                      | toksMatch (pred i) (pred j) de =
                        min (iupleft, supleft + prio (pred i))
                      | otherwise = id
                    res = keep $ min (succ iup, sup) (succ ileft, sleft)
                 in res : go (succ j) res
    vecE = vec -- "backward" operation
      where
        vec i
          | i == e = deVE de
          | i < e = V.fromList . reverse . upd i . vec $ succ i
          | otherwise = error "Internal bounds check failure"
        upd i v = (deL de - i, 0) : go (pred $ deW de) (deL de - i, 0)
          where
            go j (idown, sdown)
              | j < 0 = []
              | otherwise =
                let (iright, sright) = v V.! j
                    (idownright, sdownright) = v V.! succ j
                    keep
                      | toksMatch i j de =
                        min (idownright, sdownright + prio i)
                      | otherwise = id
                    res = keep $ min (succ idown, sdown) (succ iright, sright)
                 in res : go (pred j) res
    scoreAdd (l1, x1) (l2, x2) = (l1 + l2, x1 + x2)
    {- Now, find the optimal point for splitting.
     -
     - Heuristics A: if 2 paths are completely same, prefer deletion first;
     - which is done by choosing the 'upper' of two possibilities
     - preferentially (or 'lower' ie 'more to the right' in case of transposed
     - matrix) -}
    opt =
      (a +) .
      (if trans
         then minIndexRev
         else minIndexFwd) $
      V.zipWith scoreAdd (slice vecSmid) (slice vecEmid)
      where
        slice = V.slice a (succ $ b - a)
    diff
      | s > e =
        error $
        "Internal failure -- recursion off limits: " <>
        show s <> " vs " <> show e
      | s == e = map (\i -> (Add, deT2 de V.! i)) [a .. pred b]
      | succ s == e =
        let vecLS = deVS de
            vecRE = deVE de
            vecLE = vecE s
            vecRS = vecS e
            sumL = V.zipWith scoreAdd vecLS vecLE
            sumR = V.zipWith scoreAdd vecRS vecRE
            {- This is getting a bit complicted. In the non-transposed case, we
             - want to select one Remove/Keep surrounded by 0-n Add ops, possibly
             - from both sides. The chosen path must belong to the best paths
             - (bidirectional matrix sums must match the minimum at (s,a) and
             - (b,e) on all steps), AND at the same time the path must be
             - admissible for the edit operations (ie, it has to `backtrack
             - well`). Also, it should follow Heuristic A that says that Remove
             - and Keep ops should go earlier than Add ops (or vice versa if
             - transposed). -}
            totalCost = sumL V.! a
            sCost = vecLS V.! a
            eCost = vecRS V.! b
            doKeep
              | fst eCost - fst sCost == succ (b - a) = False
              | fst eCost - fst sCost == pred (b - a) = True
              | otherwise =
                error $
                "Internal check failure -- costs seem broken: " <>
                show [sCost, eCost] <> show [a, b]
            jumpPos =
              (if trans {- Heuristic A applies here -}
                 then last
                 else head)
                [ i
                | i <-
                    [a .. if doKeep
                            then pred b
                            else b]
                , fst (vecLS V.! i) == fst sCost - a + i
                , sumL V.! i == totalCost
                , if doKeep
                    then scoreAdd (vecLS V.! i) (0, prio s) ==
                         vecRS V.! succ i
                    else scoreAdd (vecLS V.! i) (1, 0) == vecRS V.! i
                , if doKeep
                    then sumR V.! succ i == totalCost
                    else sumR V.! i == totalCost
                , not doKeep || toksMatch s i de
                ]
            jumpEnd =
              if doKeep
                then succ jumpPos
                else jumpPos
         in map
              (\i ->
                 ( if trans
                     then Remove
                     else Add
                 , deT2 de V.! i))
              [a .. pred jumpPos] ++
            [ if trans
                then if doKeep
                       then (Keep, deT2 de V.! jumpPos)
                       else (Add, deT1 de V.! s)
                else ( if doKeep
                         then Keep
                         else Remove
                     , deT1 de V.! s)
            ] ++
            map
              (\i ->
                 ( if trans
                     then Remove
                     else Add
                 , deT2 de V.! i))
              [jumpEnd .. pred b]
      | otherwise =
        diffToks' de {deE = mid, deVE = vecEmid, deB = opt} ++
        diffToks' de {deS = mid, deVS = vecSmid, deA = opt}
