{-# LANGUAGE Rank2Types #-}
{-# LANGUAGE TupleSections #-}

module Tokenizers (TokOpts, tokOptions, tokenize) where

import qualified Data.ByteString as B
import qualified Data.ByteString.UTF8 as BU8
import qualified Data.Word8 as W8
import Options.Applicative
import Types
import qualified Unicode.Char.General as UC

data TokOpts =
  TokOpts
    { optTokenizerName :: String
    }
  deriving (Show)

tokOptions :: Parser TokOpts
tokOptions =
  TokOpts <$>
  strOption
    (metavar "LEXER" <>
     short 'l' <>
     long "lexer" <>
     value "text" <>
     help "Lexer name (use --help-lexers to list available ones)")

--TODO this should later allow choosing the lexer by suffix
tokenize :: MonadFail m => TokOpts -> FilePath -> BS -> m [Tok]
tokenize topt _ =
  case filter ((== optTokenizerName topt) . tkName) tokenizers of
    [t] -> runTokenizer t
    _ -> const $ fail "Lexer not found"

data Tokenizer =
  Tokenizer
    { tkName :: String
    , tkDescription :: String
    , runTokenizer :: forall m. MonadFail m =>
                                  BS -> m [Tok]
    }

tokenizers :: [Tokenizer]
tokenizers =
  [ Tokenizer "lines" "works like the traditional diff" tokenizeLines
  , Tokenizer "asciiwords" "separate by ASCII whitespace" tokenizeWords8
  , Tokenizer "words" "separate on any UTF8 spacing" tokenizeWords
  , Tokenizer
      "text"
      "separate groups of similar-class UTF8 characters"
      tokenizeUnicode
  ]

tokenizeLines :: MonadFail m => BS -> m [Tok]
tokenizeLines = pure . map (True, ) . BU8.lines'

tokenizeWords8 :: MonadFail m => BS -> m [Tok]
tokenizeWords8 =
  pure . map (\x -> (not $ W8.isSpace $ B.head x, x)) . bsGroupOn W8.isSpace
  where
    bsGroupOn f = B.groupBy (\l r -> f l == f r)

tokenizeWords :: MonadFail m => BS -> m [Tok]
tokenizeWords = pure . map makeTokenBU8 . groupOnBU8 UC.isWhiteSpace

makeTokenBU8 :: BS -> Tok
makeTokenBU8 s = if
  maybe False (not . UC.isWhiteSpace . fst) (BU8.decode s)
  then (True, s)
  else (False, s)

{- needed?
foldri :: (Char -> Int -> a -> a) -> a -> BS -> a
foldri cons nil cs =
  case decode cs of
    Just (a, l) -> cons a l (foldrS cons nil $ drop l cs)
    Nothing -> nil
-}

spanBU8 :: (Char -> Bool) -> BS -> (BS, BS)
spanBU8 f orig = B.splitAt (go 0 orig) orig
  where
    go :: Int -> BS -> Int
    go n bs =
      case BU8.decode bs of
        Just (a, l) ->
          if f a
            then go (n + l) (B.drop l bs)
            else n
        Nothing -> n

groupByBU8 :: (Char -> Char -> Bool) -> BS -> [BS]
groupByBU8 f s =
  case BU8.decode s of
    Just (a, _) ->
      let (g, s') = spanBU8 (f a) s
       in g : groupByBU8 f s'
    Nothing -> []

groupOnBU8 :: Eq a => (Char -> a) -> BS -> [BS]
groupOnBU8 f = groupByBU8 (\l r -> f l == f r)

tokenizeUnicode :: MonadFail m => BS -> m [Tok]
tokenizeUnicode = pure . map makeTokenBU8 . groupOnBU8 simpleUnicodeCategory

simpleUnicodeCategory :: Char -> Int
simpleUnicodeCategory c
  | UC.isWhiteSpace c = 1
  | UC.isAlphaNum c = 2
  | UC.isSymbol c = 3
  | UC.isPunctuation c = 4
  | UC.isSeparator c = 5
  | otherwise = 0
