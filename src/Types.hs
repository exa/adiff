--{-# LANGUAGE BangPatterns #-}

module Types where

import Data.ByteString
import Data.Vector

type BS = ByteString

{- TODO: all this needs to get unboxed -}
type Tok = (Bool, BS)

type TV = Vector Tok

type Diff = [(Op, Tok)]

type Hunk = ((Int, Int), Diff)

data Op
  = Remove
  | Keep
  | Add
  | MineChanged
  | Original
  | YourChanged
  deriving (Show, Eq)

data Origin
  = Stable
  | Mine
  | Your
  deriving (Show, Eq)
