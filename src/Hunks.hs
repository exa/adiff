module Hunks where

import Data.List.Extra
import Types

hunks :: Int -> Diff -> [Hunk]
hunks ctxt d =
  map (stripNums . map snd) .
  filter (not . null) . split fst . zip remove . addNums $
  d
  where
    edit (Keep, _) = 0
    edit _ = 1
    edits :: [Int]
    edits = tail $ scanl (+) 0 (map edit d)
    padEnd _ [] = []
    padEnd i [a] = a : replicate i a
    padEnd i (x:xs) = x : padEnd i xs
    remove =
      drop ctxt $
      zipWith (<=) (padEnd ctxt edits) (replicate (1 + 2 * ctxt) 0 ++ edits)
    addNums = snd . mapAccumL countTok (0, 0)
    stripNums = (,) <$> fst . head <*> map snd
    countTok x@(i, j) d'@(op, _) =
      (,)
        (case op of
           Remove -> (i + 1, j)
           Keep -> (i + 1, j + 1)
           Add -> (i, j + 1)
           MineChanged -> (i, j)
           Original -> (i + 1, j + 1)
           YourChanged -> (i, j))
        (x, d')
