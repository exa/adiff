{-# LANGUAGE CPP #-}

module Version where

import Options.Applicative

adiffVersion :: String
adiffVersion = VERSION_adiff

versionOption :: String -> Parser (a -> a)
versionOption prog =
  infoOption
    (prog <>
     " version " <>
     adiffVersion <>
     "\nCopyright (C) 2020 Mirek Kratochvíl <exa.exa@gmail.com>\nThis is free software, you are free to change and redistribute it under the terms of GPLv3.\nThere is NO WARRANTY, to the extent permitted by law.\n\nAdapted from the original version by Arek Antoniewicz.")
    (short 'v' <> long "version" <> help "display version information")

helperOption :: Parser (a -> a)
helperOption =
  abortOption
    (ShowHelpText Nothing)
    (long "help" <> short 'h' <> help "Show this help text")
